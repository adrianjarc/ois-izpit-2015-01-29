var express = require('express'), path = require('path'), fs = require('fs');

var app = express();
app.use(express.static(__dirname + '/public'));

var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Zunanja avtentikacija)
 */
app.get('/api/prijava', function(req, res) {
	// ...
	var uporabniskoIme = req.query.uporabniskoIme;
	var geslo = req.query.geslo;
	var uspesno = preveriSpomin(uporabniskoIme, geslo) || preveriDatotekaStreznik(uporabniskoIme, geslo);

	var napaka = "";
	if (uporabniskoIme == null || uporabniskoIme.length == 0 || geslo == null || geslo.length == 0)
		napaka = "Napačna zahteva.";
	else if (!uspesno) {
		napaka = "Avtentikacija ni uspela.";
	}

	res.send({status: uspesno, napaka: napaka});
	// ...
});


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (PU Prijava uporabnika v sistem)
 */
app.get('/prijava', function(req, res) {
	// ...
	var uname = req.query.uporabniskoIme;
	var pass = req.query.geslo;
	
	var a = false;
					
	a = preveriSpomin(uname, pass) || preveriDatotekaStreznik(uname, pass);
					
	if (a == true){
		res.send("<html><title>Uspešno</title><body><p>Uporabnik <b>" + uname + "</b> uspešno prijavljen v sistem!</p></body></html>");
	} else {
		res.send("<html><title>Neuspešno</title><body><p>Uporabnik <b>" + uname + "</b> nima pravice prijave v sistem!</p></body></html>");
	}
	// ...
});


var port = process.env.PORT || 3030;
app.listen(port);
console.log('Streznik tece na ' + port + ' ...');


var podatkiSpomin = ["admin/nimda", "gost/gost"];


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti (branje datoteka na strani strežnika)
 */
var podatkiDatotekaStreznik = JSON.parse(fs.readFileSync('./public/podatki/uporabniki_streznik.json', 'utf8')); //require("./public/podatki/uporabniki_streznik.json");

//console.log(podatkiDatotekaStreznik);

/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriSpomin(uporabniskoIme, geslo) {
	// ...
	for (var i=0; i<podatkiSpomin.length; i++){
		var pod = podatkiSpomin[i].split("/");
		if (uporabniskoIme == pod[0] && geslo == pod[1]){
			return true;
		}
	}
	return false;
	// ...
}


/**
 * TODO: Potrebna je implementacija tega dela funkcionalnosti
 */
function preveriDatotekaStreznik(uporabniskoIme, geslo) {
	// ...
	for (var user in podatkiDatotekaStreznik){
		if (uporabniskoIme == user.uporabnik && geslo == user.geslo){
			return true;
		}
	}
	return false;
	// ...
}